#!/bin/bash

# VARIABLES
# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
echo $SCRIPTPATH

cd $SCRIPTPATH/../../

if [ -d "pkg/filesystem/" ]; then
  cp --no-target-directory -r pkg/filesystem/ /
  systemd-tmpfiles --create
fi

python3 -m venv venv
. venv/bin/activate

pip install --upgrade pip
pip install --upgrade wheel

pip install --no-deps --no-binary lxml -r requirements.txt

python3 manage.py migrate --noinput
python3 manage.py createinitialrevisions || true
python3 manage.py database_files_cleanup || true
python3 manage.py celery_sync_all_tasks || true
#python3 manage.py ldap_sync_users || true
python3 manage.py collectstatic --noinput || true
python3 manage.py compilemessages -i "venv*" || true

find ./ -type f -size 0 -name django.po -exec rm -f {} \;
find ./ -type f -size 0 -name django.mo -exec rm -f {} \;

chown -R www-data:www-data ./

if [ -d "pkg/filesystem/etc/systemd/system" ]; then
  unit_files=`ls pkg/filesystem/etc/systemd/system`
  systemctl daemon-reload
  systemctl enable $unit_files --now
  systemctl restart $unit_files
fi
